#! /usr/bin/env zsh

# dnf setup
sudo cp /etc/dnf/dnf.conf /etc/dnf/dnf.conf.bak_$(date)
sudo echo -e "
# speed
fastestmirror=True
max_parallel_downloads=10
defaultyes=True
keepcache=True" >> /etc/dnf/dnf.conf


# rpm setup
sudo dnf update
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf groupupdate -y core


# flathub setup
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

# multimedia codecs
sudo dnf groupupdate -y multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin

# desktop environment
sudo dnf install -y gnome-tweaks gnome-extensions

mkdir build
git clone https://github.com/micheleg/dash-to-dock.git build/dash-to-dock
cd build/dash-to-dock && {
   make
   make install
}
cd ../..


# nvim
https://gitlab.com/samlee91/nvim.git ~/.config/nvim


# users
echo -e "
\e[32m Add password for Lincoln"
sudo useradd -m lincoln
sudo passwd lincoln


echo -e "
\e[32mDone. Reboot the system"

